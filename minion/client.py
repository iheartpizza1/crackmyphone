""" Minion's http client module. This is the starting point for the minion.
"""
from http.client import HTTPConnection
from multiprocessing import Pool as ThreadPool, Manager, Lock, Process
import json
import itertools
import logging
import time
import random
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))
from lib.cracker import Cracker, PasswordFound, PasswordNotFound
from tools.tools import ValidateJSON, PasswordRange, RangeGenerator
from tools.cypher import Base64CypherCreator
from tools.persistence import JSONFilePersistor

# Global vars
MASTER_SERVER = "localhost:8080"
INSTRUCTION = ""
THREAD_COUNT = 1
_cypher = Base64CypherCreator()

def GetInstructions():
    """ Get instructions from master. If received 'nothing_new', sleep want try again.
    """
    uri = "/instruction"
    conn = HTTPConnection(MASTER_SERVER)

    conn.request("GET", uri)
    response = conn.getresponse()

    data = __extract_response_json(response, conn)

    # If the JSON did not pass validation, return error to minion
    if not data:
        logging.error('Received bad data from master')
    elif 'nothing_new' in data:
        logging.info('Nothing to process from master')
        logging.debug("Sleeping before request for instructions")
        time.sleep(random.randint(0, 15))
    else:
        __setup_new_instruction(data)

def __extract_response_json(response, connection):
    """ Extract response to a JSON dict

    Returns:
        dict -- JSON parse result after validation, or None if validation failed
    """
    try:
        data = response.read()
        logging.debug("Encoded msg: %s", data)
        json_data = _cypher.decode(data)
        return ValidateJSON(json_data)
    except Exception:
        logging.error("Error reading master's response")
    finally:
        connection.close()

def __setup_new_instruction(json_data):
    """ Save the received instruction and start cracking.

    Arguments:
        json_data {dict} -- Instructions from master with the keys:

            'target_hash' -- The hash which needs to be cracked
            'range_start' -- Start of the password range
            'range_stop' -- End of the password range
    """
    # Save instruction for identification purposes
    global INSTRUCTION
    INSTRUCTION = str(json.dumps(json_data))

    logging.info("Received instruction: %s", INSTRUCTION)
    pw_range = PasswordRange(int(json_data['range_start']),
                             int(json_data['range_stop']),
                             str(json_data.get('prefix', PasswordRange.DEFAULT_PREFIX)))

    results = __distribute_load(target_hash=(json_data.get('target_hash')), pw_range=pw_range)

    for result in results:
        if result:
            temp = json.loads(INSTRUCTION)
            temp['correct_pw'] = result
            completed_instruction = json.dumps(temp)
            __send_report(completed_instruction, pw_found=True)
            return
    __send_report(INSTRUCTION)

def __distribute_load(target_hash, pw_range):
    """ Distribute the password range workload to processes. The number of processes
    is set by the 'THREAD_COUNT' variable, as read from the 'minion/conf.json' file.
    Default value is 1.

    Arguments:
        target_hash {str} -- The hash which needs to be cracked
        pw_range {PasswordRange} -- The range of passwords to be checked

    Returns:
        list -- List of results from the processes. May contain:
            Password {str} -- The correct password
            None -- If the password wasn't found
    """
    ranges = []
    increment = (pw_range.stop - pw_range.start) // THREAD_COUNT

    if THREAD_COUNT == 1:
        ranges.append(pw_range)
    else:
        # Make THREAD_COUNT-1 slices of ranges
        for i in range(1, THREAD_COUNT):
            ranges.append(PasswordRange(pw_range.start + (increment * (i - 1)),
                                        pw_range.start + (increment * i),
                                        pw_range.prefix))

        # For the last slice, if there's divison leftovers, add the slice with the leftovers
        # to the range list
        i += 1
        ranges.append(
            PasswordRange(pw_range.start + (increment * (i - 1)) +
                          ((pw_range.stop - pw_range.start) % THREAD_COUNT),
                          pw_range.start + (increment * i) +
                          ((pw_range.stop - pw_range.start) % THREAD_COUNT),
                          pw_range.prefix))

    with Manager() as manager:
        status_dict = manager.dict()
        lock = Lock()

        # Set a global status for all processes to check
        status_dict['PasswordFound'] = 0

        if logging.RootLogger.manager.root.level == logging.DEBUG:
            waiting_p = Process(target=__print_status, args=(status_dict, lock))
            waiting_p.start()
        pool = ThreadPool(THREAD_COUNT)
        results = pool.starmap(__start_cracking,
                               zip(itertools.repeat(status_dict),
                                   itertools.repeat(target_hash),
                                   ranges))

        pool.close()
        pool.join()
        if logging.RootLogger.manager.root.level == logging.DEBUG:
            waiting_p.terminate()
            logging.debug("Results: %s", results)

        return results

def __start_cracking(status_dict, target_hash, pw_range):
    """ Create instance of Cracker and start comparing passwords in the range.

    Arguments:
        status_dict {synced dict} -- Dict synced between the processes containing
        their statuses
        target_hash {str} -- The hash to crack
        pw_range {PasswordRange} -- The range of passwords to be checked

    Returns:
        str -- The correct password if found, or None if not found
    """
    cracker = Cracker(status_dict, target_hash, pw_range)

    # Start cracking and wait for results to be raised
    try:
        cracker.start()
    except PasswordFound:
        return cracker.correct_pw
    except PasswordNotFound:
        return None

def __send_report(instruction, pw_found=False):
    """ Send report back to master.

    Arguments:
        instruction {string} -- The instruction this minion just executed

    Keyword Arguments:
        pw_found {bool} -- Whether the password was found (default: {False})
    """
    if not pw_found:
        uri = "/notfound"
    else:
        uri = "/found"
    logging.info("Sending report: %s", instruction)
    instruction = _cypher.encode(instruction)
    conn = HTTPConnection(MASTER_SERVER)
    try:
        conn.request("POST", uri, body=instruction)
        conn.getresponse()
    except ConnectionError as e:
        cant_connect = True
        while cant_connect:
            logging.error("Error reporting to master, he's dead.")
            time.sleep(1)
            try:
                conn = HTTPConnection(MASTER_SERVER)
                conn.request("POST", uri, body=instruction)
                conn.getresponse()
                cant_connect = False
                logging.info("Report sent. Master is back!")
            except Exception:
                cant_connect = True
    except Exception as e:
        logging.error("Something weird happened: %s", e)

    conn.close()

def __print_status(status_dict, lock):
    """ Periodically print the calculation status of the processes.

    Arguments:
        status_dict {synced dict} -- Dict synced between the processes containing their statuses
        lock {multiprocess.Lock} -- Process lock for printing nicely
    """
    while status_dict['PasswordFound'] == 0:
        lock.acquire()
        try:
            for k, v in status_dict.items():
                logging.debug("Status: %s:%s", k, v)
        finally:
            lock.release()
        time.sleep(1)

def __load_conf():
    """ Load a 'conf.json' file, and distribute global attributes.

    Returns:
        dict -- kwargs read from the conf file
    """
    conf_persistor = JSONFilePersistor(os.path.join(os.path.dirname(__file__), 'conf.json'))
    conf_dict = conf_persistor.ReadJSON()
    if not conf_dict:
        return None

    if 'server' in conf_dict:
        global MASTER_SERVER
        MASTER_SERVER = conf_dict['server']

    if 'thread_count' in conf_dict:
        global THREAD_COUNT
        THREAD_COUNT = conf_dict['thread_count']
    return conf_dict

def _parse_args(log_kwargs):
    """ Parse script's args for logger settings.

    Arguments:
        log_kwargs {dict} -- kwargs for the logger, set first by the conf file.

    Raises:
        ValueError -- When an arg is missing its value or is invalid for the logger' settings

    Returns:
        dict -- Modified log_kwargs for the logger
    """
    numeric_level = log_kwargs['level']
    if "--log" in sys.argv:
        try:
            for i in RangeGenerator(1, len(sys.argv)):
                if (sys.argv[i] == "--log" and
                        i+1 < len(sys.argv) and
                        sys.argv[i+1]):
                    loglevel = sys.argv[i+1]
                    break
        except IndexError:
            raise ValueError('Invalid log level')
        if loglevel:
            numeric_level = getattr(logging, loglevel.upper(), None)
            if not isinstance(numeric_level, int):
                raise ValueError('Invalid log level: %s' % loglevel)
    log_kwargs['level'] = numeric_level
    if "--logfile-name" in sys.argv:
        try:
            for i in RangeGenerator(1, len(sys.argv)):
                if (sys.argv[i] == "--logfile-name" and sys.argv[i+1]):
                    log_kwargs['filename'] = sys.argv[i+1]
                    break
        except IndexError:
            raise ValueError('Invalid log file name')

    return log_kwargs

def main():
    """ Main function for the Minion. Load settings from the conf file,
    check for user arguments for the logger, and start asking the master for instructions.
    """
    conf_dict = __load_conf()

    log_kwargs = {}
    if "loglevel" in conf_dict:
        log_kwargs['level'] = getattr(logging, conf_dict['loglevel'].upper(), None)
    if "filename" in conf_dict:
        log_kwargs['filename'] = conf_dict['filename']
    if "format" in conf_dict:
        log_kwargs['format'] = conf_dict['format']

    log_kwargs = _parse_args(log_kwargs)
    logging.basicConfig(**log_kwargs)

    while True:
        try:
            GetInstructions()
        except Exception as e:
            logging.error("Failed to contact master. Exception: %s", e)

if __name__ == '__main__':
    main()
