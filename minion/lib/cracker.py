""" Module containing password cracking capabilities.
"""
import logging
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))
from tools.tools import RangeGenerator, CrackPW

class PasswordFound(Exception):
    """ Exception for when a password was found in a given range.
    """

class PasswordNotFound(Exception):
    """ Exception for when a password wasn't found in a given range.
    """

class Cracker:
    """ Class in charge of cracking a password in a given range.
    Meant to be run in a process.

    Raises:
        PasswordFound -- When a correct password is found
        PasswordNotFound -- When a password wasn't found
    """
    def __init__(self, status_dict, target_hash, pw_range):
        self.status_dict = status_dict
        self.target_hash = target_hash
        self.pw_range = pw_range
        self.correct_pw = ''
        self.identifier = str("%d-%d" % (self.pw_range.start, self.pw_range.stop))

        # Insert my value in the status_dict
        self.status_dict[self.identifier] = 0

    def start(self):
        """ Start going down the given range and compare passwords.

        Raises:
            PasswordFound -- When a correct password is found
            PasswordNotFound -- When a password wasn't found
        """
        for i in RangeGenerator(self.pw_range.start, self.pw_range.stop):
            # If password was already found, exit
            if self.status_dict['PasswordFound'] == 1:
                break

            # Update status_dict with current index
            self.status_dict[self.identifier] = i

            curr_pw = self.pw_range.getPassword(i)
            match_found = CrackPW(curr_pw, self.target_hash)

            if match_found:
                self.correct_pw = curr_pw
                self.status_dict['PasswordFound'] = 1
                logging.info("Found it! %s", self.correct_pw)
                raise PasswordFound
        raise PasswordNotFound
