import unittest
import os
import sys
import logging
sys.path.append(os.path.join(os.path.dirname(__file__), '../'))
from tools.persistence import FilePersistor, JSONFilePersistor

class FilePersistor_Test(unittest.TestCase):
    """ Test FilePersistor's methods """

    def __init__(self, *args, **kwargs):
        super(FilePersistor_Test, self).__init__(*args, **kwargs)
        self.file_persistor = FilePersistor('test_file.txt')

    def test_read_file_doesnt_exist(self):
        result = self.file_persistor.Read()
        self.assertIsNone(result, "Expected none since file doesn't exist")

    def test_delete_file_doesnt_exist(self):
        result = self.file_persistor.DeleteFile()
        self.assertTrue(result, "Expected True since file doesn't exist")
        result = self.file_persistor.DeleteFile()
        self.assertTrue(result, "Expected True since file doesn't exist")

    def test_write_file_doesnt_exist(self):
        result = self.file_persistor.Write('Cyka Blyat')
        self.assertTrue(result, "Expected True since file should be created and written ok")

    def test_write_file_exists(self):
        result = self.file_persistor.Write('Bochka Bass')
        self.assertTrue(result, "Expected True since file should be overwritten ok")

    def test_delete_file_exists(self):
        result = self.file_persistor.DeleteFile()
        self.assertTrue(result, "Expected True for file deletion success")

    def test_write_file_overwrite(self):
        data = 'Bochka Bass'
        result = self.file_persistor.Write(data)
        
        if result:
            content = self.file_persistor.Read()
            self.assertEqual(content, data, "Expecting current file data to be the same as overwritten in this test")

    def test_append_file_exists(self):
        olddata = self.file_persistor.Read()
        data = 'Kolbasor'
        result = self.file_persistor.Append(data)
        
        if result:
            curr_content = self.file_persistor.Read()
            self.assertEqual(curr_content, str(olddata) + str(data), "Expected '%s', got %s" % (str(olddata) + str(data), curr_content))
    
    def test_append_file_not_exists(self):
        self.file_persistor.DeleteFile()
        data = 'Wat'
        result = self.file_persistor.Append(data)
        
        if result:
            curr_content = self.file_persistor.Read()
            self.assertEqual(curr_content, data, "Expected '%s', got %s" % (data, curr_content))

class JSONFilePersistor_Test(unittest.TestCase):
    """ Test JSONFilePersistor's methods """

    def __init__(self, *args, **kwargs):
        super(JSONFilePersistor_Test, self).__init__(*args, **kwargs)
        self.json_file_persistor = JSONFilePersistor('json_test_file.json')

    def test_write_json(self):
        json_dict = {
            "test" : "data",
            "number" : 123
        }
        result = self.json_file_persistor.WriteJSON(json_dict)
        self.assertTrue(result, "Expected True, got %s" % (result))

    def test_read_json(self):
        json_dict = {
            "test" : "data",
            "number" : 123
        }
        if self.json_file_persistor.WriteJSON(json_dict):
            result = self.json_file_persistor.ReadJSON()
            self.assertIsNotNone(result, "Expected result not none since file exists and has content")
            self.assertEqual(result, json_dict, "Expected %s, got %s" % (json_dict, result))

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
