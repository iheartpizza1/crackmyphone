import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../'))
sys.path.append(os.path.join(os.path.dirname(__file__), '../master'))
import unittest
import threading
import logging
from http.client import HTTPConnection
import json
import time
from tools.tools import ValidateJSON, MIN_PASSWORD, MAX_PASSWORD
from master import server
from tools.cypher import Base64CypherCreator

MASTER_SERVER = "192.168.1.103:8080"
INSTRUCTION = ""
_cypher = Base64CypherCreator()

class MasterTest_Found(unittest.TestCase):
    """ Test the ./found node on the Master server """
    
    """ EXPECTING FAIL """
    def test_bad_targethash(self):
        data = {
            "target_hash" : "blablabla",
            "range_start" : MIN_PASSWORD,
            "range_stop" : MAX_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_negative_range_start(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MIN_PASSWORD - (MIN_PASSWORD + 1),
            "range_stop" : MAX_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_huge_range_start(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MAX_PASSWORD + 1,
            "range_stop" : MAX_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_negative_range_stop(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MIN_PASSWORD,
            "range_stop" : MIN_PASSWORD - (MIN_PASSWORD + 1)
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_huge_range_stop(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MIN_PASSWORD,
            "range_stop" : MAX_PASSWORD + 1
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_equal_ranges(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : 42,
            "range_stop" : 42
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_ranges_start_gt_stop(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MAX_PASSWORD,
            "range_stop" : MIN_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_target_hash(self):
        data = {
            "range_start" : MIN_PASSWORD,
            "range_stop" : MIN_PASSWORD + 1
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_range_start(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_stop" : MIN_PASSWORD + 1
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_range_stop(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MIN_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_ranges(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27"
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_hash_and_start(self):
        data = {
            "range_stop" : MAX_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_hash_and_stop(self):
        data = {
            "range_start" : MIN_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_everything(self):
        data = { }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    """ EXPECTING SUCCESS """
    def test_range_start_zero(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : 0,
            "range_stop" : MAX_PASSWORD,
            "correct_pw" : "050-1234567"
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 200, "Expected to pass validation on Master and return 200")
    
    def test_range_stop_one(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MIN_PASSWORD,
            "range_stop" : MIN_PASSWORD + 1,
            "correct_pw" : "050-1234567"
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 200, "Expected to pass validation on Master and return 200")
    
    def test_real_success(self):
        """
        In real life, this test will run after testing more cases where first a request
        for instructions is sent, analyzed, and fucked with.
        To save time and bring more features to this table, I opted to go straghit for a meaty test,
        just as a POC.
        """
        
        # "051-0000000" "2709f45aee0fb29707e9a847e4d8c9c4"
        # "050-8126459" "957d220a16bdafb291d22bd677b7fd27"
        # "050-0000000" "1d0b28c7e3ef0ba9d3c04a4183b576ac"

        # Emulate request for instructions
        response, conn = _contact_master("/instruction", "GET", "")
        identifier = _extract_response_json(response, conn)
        
        identifier['correct_pw'] = "050-0000000"
        identifier['target_hash'] = "1d0b28c7e3ef0ba9d3c04a4183b576ac"
        # identifier['correct_pw'] = "051-0000000"
        # identifier['target_hash'] = "2709f45aee0fb29707e9a847e4d8c9c4"
        time.sleep(1)
        result, conn = self._send_data(json.dumps(identifier))
        self.assertEqual(result.status, 200, "Expected to pass validation on Master and return 200")
    
    """ HELPING METHOD """
    def _send_data(self, json_string):
        uri = "/found"
        method = "POST"

        return _contact_master(uri, method, json_string)

class MasterTest_NotFound(unittest.TestCase):
    """ Test the ./notfound node on the Master server """
    
    """ EXPECTING FAIL """
    def test_bad_targethash(self):
        data = {
            "target_hash" : "blablabla",
            "range_start" : MIN_PASSWORD,
            "range_stop" : MAX_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_negative_range_start(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MIN_PASSWORD - (MIN_PASSWORD + 1),
            "range_stop" : MAX_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_huge_range_start(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MAX_PASSWORD + 1,
            "range_stop" : MAX_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_negative_range_stop(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MIN_PASSWORD,
            "range_stop" : MIN_PASSWORD - (MIN_PASSWORD + 1)
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_huge_range_stop(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MIN_PASSWORD,
            "range_stop" : MAX_PASSWORD + 1
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_equal_ranges(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : 42,
            "range_stop" : 42
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 200, "Expected to pass validation on Master and return 400")
    
    def test_ranges_start_gt_stop(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MAX_PASSWORD,
            "range_stop" : MIN_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_target_hash(self):
        data = {
            "range_start" : MIN_PASSWORD,
            "range_stop" : MIN_PASSWORD + 1
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_range_start(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_stop" : MIN_PASSWORD + 1
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_range_stop(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MIN_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_ranges(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27"
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_hash_and_start(self):
        data = {
            "range_stop" : MAX_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_hash_and_stop(self):
        data = {
            "range_start" : MIN_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    def test_missing_everything(self):
        data = { }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 400, "Expected to fail validation on Master and return 400")
    
    """ EXPECTING SUCCESS """
    def test_range_start_zero(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : 0,
            "range_stop" : MAX_PASSWORD
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 200, "Expected to pass validation on Master and return 200")
    
    def test_range_stop_one(self):
        data = {
            "target_hash" : "957d220a16bdafb291d22bd677b7fd27",
            "range_start" : MIN_PASSWORD,
            "range_stop" : MIN_PASSWORD + 1
        }

        result, conn = self._send_data(json.dumps(data))
        self.assertEqual(result.status, 200, "Expected to pass validation on Master and return 200")
    
    """ HELPING METHOD """
    def _send_data(self, json_string):
        uri = "/notfound"
        method = "POST"

        return _contact_master(uri, method, json_string)

"""
Helper client
"""
def _contact_master(uri, method, body):
    # Define connection to Master
    conn = HTTPConnection(MASTER_SERVER)
    body = _cypher.encode(body)

    # Make request for new instruction to master and get response
    conn.request(method, uri, body)
    response = conn.getresponse()

    # Return response
    return response, conn

def _extract_response_json(response, connection):
    # Read data
    try:
        data = response.read()
        json_data = _cypher.decode(data)
    except:
        logging.error("Error reading master's response")
    finally:
        connection.close()

    # Validate json and return it as dict
    return ValidateJSON(json_data, logging)

if __name__ == '__main__':
    logging.basicConfig(level=logging.FATAL)
    master = threading.Thread(target=server.main)
    master.start()
    time.sleep(10)
    # MasterTest_Found().test_real_success()
    unittest.main()
    exit()
