""" Module for cypher implementations
"""

import abc
import base64
import json

class AbsCypherCreator(metaclass=abc.ABCMeta):
    """ Factory Method design pattern for diverse methods of encoding/decoding data for
    web transmission
    """
    def __init__(self):
        self.cypher = self._factory_method()

    @abc.abstractmethod
    def _factory_method(self):
        pass

    def encode(self, data):
        """ Default factory cypher's encode method.

        Arguments:
            data -- Data to encode

        Returns:
            Encoded data
        """
        return self.cypher.encode(data)

    def decode(self, data):
        """ Default factory cypher's decode method.

        Arguments:
            data -- Data to decode

        Returns:
            Decoded data
        """
        return self.cypher.decode(data)

class Base64CypherCreator(AbsCypherCreator):
    """ Cypher object creator (Factory Method design pattern)
    """
    def _factory_method(self):
        return Base64Cypher()

class BlowFishCypherCreator(AbsCypherCreator):
    def _factory_method(self):
        return BlowFishCypher()

class Cypher(metaclass=abc.ABCMeta):
    """ Interface for encoding and decoding data for network transmission
    """
    @abc.abstractmethod
    def encode(self, data):
        pass

    @abc.abstractmethod
    def decode(self, data):
        pass

class Base64Cypher(Cypher):
    """ Cypher for encodeing and decoding strings in Base64
    """
    def encode(self, data):
        """ Encode the string in Base64.

        Arguments:
            data {str} -- The string data to be encoded

        Returns:
            str -- Encoded string
        """
        return (base64.b64encode(data.encode())).decode()

    def decode(self, data):
        """ Decoded the Base64 encoded string to normal string.

        Arguments:
            data {str} -- The string data to be decoded

        Returns:
            str -- Decoded, normal string
        """
        return (base64.b64decode(data)).decode()

class BlowFishCypher(Cypher):
    def encode(self, data):
        pass

    def decode(self, data):
        pass
