""" General functions and classes which both the Minion and the Master modules use.
"""
import json
import re
import operator
import hashlib
import logging

MIN_PASSWORD = 0
MAX_PASSWORD = 99999999

def ValidateJSON(json_data):
    """ Validate JSON payload transmitted between the minions and the master.

    Arguments:
        json_data {str} -- JSON string

    Returns:
        dict -- The parsed JSON dict if it passed validation, otherwise None.
    """
    # Try parsing the json as string and as bytes
    try:
        data = json.loads(json_data)
    except Exception as e:
        logging.error("Error parsing json string. Exception: %s", e)
        try:
            data = json.load(json_data)
        except Exception as e:
            logging.error("Error parsing json bytes. Exception: %s", e)
            return None

    # Count which keys are present in the JSON
    checks = {
        'target_hash' : 0,
        'range_start' : 0,
        'range_stop' : 0,
        'correct_pw' : 0,
        'nothing_new' : 0
    }
    for key in data.keys():
        checks[key] = 1

    if logging.RootLogger.manager.root.level == logging.DEBUG:
        logging.debug("JSON key presence: %s", json.dumps(checks))

    # Check if the identifier keys are present together
    if not (checks['target_hash'] and checks['range_start'] and checks['range_stop']):
        # If not, check if the 'nothing_new' key is present
        if not checks['nothing_new']:
            logging.error("Incorrect key combination in JSON data")
            return None
    else:
        try:
            # Check if the range keys can be parsed as integers
            start = int(data['range_start'])
            stop = int(data['range_stop'])

            # Check if ranges are within range
            if (start < MIN_PASSWORD or
                    start > MAX_PASSWORD or
                    stop < MIN_PASSWORD or
                    stop > MAX_PASSWORD or
                    stop < start):
                raise OutOfRangeException
        except ValueError:
            logging.error("Ranges in JSON data aren't numbers")
            return None
        except OutOfRangeException:
            logging.error("Ranges are not within the required ranges")
            return None

        # Check is target hash is a hash
        pattern = re.compile("[a-fA-F0-9]{32}")
        if not pattern.fullmatch(data['target_hash']):
            logging.error('Target hash is invalid')
            return None

        # Check 'correct_pw' validity
        if checks['correct_pw']:
            pattern = re.compile("[0-9]{3}-[0-9]{7}")
            if not pattern.fullmatch(data['correct_pw']):
                logging.error('Correct password was not a password')
                return None

    # If all checks passed, return the parsed JSON
    return data

def RangeGenerator(start, stop, step=1):
    """ Generator for index ranges (for for loops).

    Arguments:
        start {int} -- First index
        stop {int} -- Last index

    Keyword Arguments:
        step {int} -- increment by which to advance 'start' toward 'stop' (default: {1})

    Raises:
        StopIteration -- When 'start' surpases 'stop'
    """
    if step > 0:
        comparison = operator.le
    elif step < 0:
        comparison = operator.ge
    else:
        raise StopIteration

    while comparison(start, stop):
        yield start
        start += step

class PasswordRange:
    """ A range of numbers to check when cracking a password.
    """
    DEFAULT_PREFIX = "05"

    def __init__(self, start, stop, prefix=DEFAULT_PREFIX):
        self.start = start
        self.stop = stop
        self.prefix = prefix # Will look like '05'

    def getPassword(self, index):
        """ Convert a password made from the given index to a phone number format.

        Arguments:
            index {int} -- An index representing a password

        Returns:
            str -- The password in a phone number format
        """
        return self.prefix + str(index // 10000000) + "-" + str(index % 10000000).zfill(7)

class OutOfRangeException(Exception):
    """ Exception to be raised on JSON validation. Raise when there's an
    issue with the 'start' and 'stop' integers in the JSON.
    """

def _get_md5_hash(password):
    """ Get the MD5 hash of the password.

    Arguments:
        password {str} -- Phone number password

    Returns:
        str -- MD5 hash of the password
    """
    hash_result = hashlib.md5(password.encode("UTF-8")).hexdigest()
    return hash_result

def CrackPW(password, target_hash):
    """ Get a password's hash and return if it matches the target hash.

    Arguments:
        password {str} -- Phone number password
        target_hash {str} -- Target hash to be cracked

    Returns:
        bool -- Whether the hashes match
    """
    compared_hash = _get_md5_hash(password)
    return target_hash == compared_hash

def GetPWRangeFromString(pw: str):
    """ Return a phone-number-like password in integer form.

    Arguments:
        pw {str} -- Password in phone number format

    Returns:
        int -- Password in integer format
    """
    return int(pw.replace('05', '', 1).replace('-', ''))
