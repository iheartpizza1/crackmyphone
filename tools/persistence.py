""" Module for persistence implementations
"""

import abc
import os
import json
from pathlib import Path
import logging

class Persistor(metaclass=abc.ABCMeta):
    """
    Interface class for persistence purposes (reading, saving and caching data)
    """

    def __init__(self):
        pass

    @abc.abstractmethod
    def Read(self):
        """ Read data """

    @abc.abstractmethod
    def Write(self, data):
        """ Write (overwrite) data """

    @abc.abstractmethod
    def Append(self, data):
        """ Append data """

class FilePersistor(Persistor):
    """
    Implementation of Persistor for file-based persistence
    """
    def __init__(self, file_path, encoding='UTF-8'):
        super().__init__()
        self.file_path = file_path
        self.encoding = encoding

    def Read(self):
        """ Open the file, read its content and return it.

        Returns:
            str -- Text content of the file.
            None -- If the file doesn't exist, or if there was an error reading it.
        """
        try:
            self._check_file_exists()
        except FileNotFoundError:
            return None

        try:
            with open(self.file_path, encoding=self.encoding) as my_file:
                # conf_dict = json.load(my_file)
                return my_file.read()
        except OSError:
            logging.error("Can't read file: %s", self.file_path)
        return None

    def Write(self, data: str):
        """ Write text data to file in overwrite mode.

        Arguments:
            data {str} -- The textual data to be written to the file

        Returns:
            True -- When writing succeeded
            False -- When opening the file, creating the file or writing failed
        """
        return self._write_to_file(data)

    def Append(self, data: str):
        """ Write text data to file in append mode. (The data will be written
        starting from the end of the file)

        Arguments:
            data {str} -- The textual data to be written to the file

        Returns:
            True -- When writing succeeded
            False -- When opening the file, creating the file or writing failed
        """
        return self._write_to_file(data, 'a')

    def DeleteFile(self):
        """ Delete the file controller by this persistor.

        Returns:
            True -- When file deletion succeeded
            False -- When file deletion failed
        """
        try:
            self._check_file_exists()
        except FileNotFoundError:
            return True

        try:
            os.remove(self.file_path)
            try:
                self._check_file_exists()
            except FileNotFoundError:
                return True
        except OSError:
            logging.error("Failed to delete myself: %s", self.file_path)
        return False

    def _write_to_file(self, data: str, mode='w'):
        """ Write text data to file. Default writing mode is overwrite mode.

        Arguments:
            data {str} -- The textual data to be written to the file

        Keyword Arguments:
            mode {str} -- Writing mode (default: {'w'}, allowed values: {'w', 'a'})

        Returns:
            True -- When writing succeeded
            False -- When opening the file, creating the file or writing failed
        """
        try:
            my_file = open(self.file_path, mode='x', encoding=self.encoding)
        except FileExistsError:
            try:
                my_file = open(self.file_path, mode=mode, encoding=self.encoding)
            except OSError:
                logging.error("Can't open file for writing: %s", self.file_path)
        try:
            chars_written = my_file.write(data)
            assert chars_written == len(data)

            return True
        except (OSError, AssertionError):
            logging.error("Can't write to file: %s", self.file_path)
        finally:
            my_file.close()
        return False

    def _check_file_exists(self):
        my_file = Path(self.file_path)
        if not my_file.is_file():
            logging.debug("File not found: %s", self.file_path)
            raise FileNotFoundError

class JSONFilePersistor(FilePersistor):
    """ File-based persistence class which reads and writes JSON strings
    """
    def __init__(self, file_path, encoding='UTF-8'):
        super().__init__(file_path, encoding)

    def ReadJSON(self):
        """ Read text from file and parse it as JSON

        Returns:
            dict -- The JSON data parsed to a dict
        """
        data = self.Read()

        if data:
            try:
                return json.loads(data)
            except Exception as e:
                logging.error("Failed parsing JSON from file: %s. Exception: %s", self.file_path, e)
        return None

    def WriteJSON(self, json_dict: dict):
        """ Write the JSON dict as a string to the file.

        Arguments:
            json_dict {dict} -- Dict that will be converted to a JSON string

        Returns:
            True -- When writing succeeded
            False -- When opening the file, creating the file or writing failed
        """
        try:
            return self.Write(json.dumps(json_dict, indent=2))
        except Exception as e:
            logging.error("Failed parsing JSON dict to string for file: %s. Exception: %s",
                          self.file_path,
                          e)
        return False

    def AppendJSON(self, json_list: list):
        """ Append new JSON data to the JSON in the file.

        Arguments:
            json_list {list} -- List to be appended to the file

        Returns:
            bool -- Wether the write to file was successful
        """
        data = self.ReadJSON()
        if data:
            lst = data
            try:
                lst.append(json_list)
            except AttributeError:
                lst = [data]
                lst.append(json_list)
        else:
            lst = [json_list]
        return self.WriteJSON(lst)
