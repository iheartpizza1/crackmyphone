""" Handle everything related to minions, progress persistence and saving of cracked passwords.
"""
import time
import json
from threading import Timer
from multiprocessing.dummy import Process, Lock
import logging
import os
import sys
import re
sys.path.append(os.path.join(os.path.dirname(__file__), '../'))
from tools.tools import PasswordRange, RangeGenerator, MIN_PASSWORD, MAX_PASSWORD, CrackPW, GetPWRangeFromString
from tools.persistence import JSONFilePersistor, FilePersistor

class Minion:
    """
    Representation of a Minion worker client.

    It's identified by:

        target_hash {str}        -- MD5 hash the minion is working on guessing

        pw_range {PasswordRange} -- Range of passwords the client is hashing against the target_hash
    """
    def __init__(self, target_hash, pw_range, timer):
        self.target_hash = target_hash
        self.pw_range = pw_range
        self.timer = timer
        self.timer.args = [self]
        self.timer.start()

    def to_dict(self):
        """ Get a dict representation of the Minion.

        Returns:
            dict -- Dict of the minion's 'target_hash', 'range_start', and 'range_stop'
        """
        return {
            'target_hash' : self.target_hash,
            'range_start' : self.pw_range.start,
            'range_stop'  : self.pw_range.stop
        }

    def to_string(self):
        """ Get a string representation on the Minion.

        Returns:
            str -- String in the format: "'target_hash':'range_start'-'range_stop"
        """
        return "%s:%s-%s" % (self.target_hash, self.pw_range.start, self.pw_range.stop)

class MinionHandler:
    """ Handler class which handles Minion requests and reports, and manages the persistence of
    the hashing progress, input hashes and output of cracked passwords.
    """
    def __init__(self, max_slices=1000, progress_flush_interval=3, input_monitor_inteval=5):
        self.minions = []
        self.ranges_todo = []
        self.found_hashes = []
        self.max_slices = max_slices
        self.minion_persistor = JSONFilePersistor(os.path.join(os.path.dirname(__file__), 'minions.json'))
        self.ranges_todo_persistor = JSONFilePersistor(os.path.join(os.path.dirname(__file__), 'ranges_todo.json'))
        self.output_persistor = JSONFilePersistor(os.path.join(os.path.dirname(__file__), 'output.json'))
        self.input_persistor = FilePersistor(os.path.join(os.path.dirname(__file__), 'input.json'))
        self.ranges_lock = Lock()
        self.minions_lock = Lock()
        self.hashes_lock = Lock()
        self.progress_flush_interval = progress_flush_interval
        self.input_monitor_interval = input_monitor_inteval
        self.flush_progress_thread = Process(target=self.__flush_progress,
                                             name="Flush Progress Thread")
        self.input_monitor = Process(target=self.__monitor_input,
                                     name="Input Monitor Thread")
        self.__load_found_hashes()
        self.__load_todo()
        self.__load_minions()
        self.__start_persistence()

    def __load_found_hashes(self):
        """ Read the 'master/output.json' file to memory
        """
        hashes = self.output_persistor.ReadJSON()
        if hashes:
            self.found_hashes = hashes

    def __load_todo(self):
        """ Read the cracking progress (todo list of password ranges for desired hashes) from
        the 'master/ranges_todo.json' file.
        """
        data = self.ranges_todo_persistor.ReadJSON()
        if data:
            with self.ranges_lock:
                self.ranges_todo = data
                if logging.RootLogger.manager.root.level == logging.DEBUG:
                    logging.debug("Loaded progress from file")
                    # Print the contents of the todo list. This can be really long.
                    # Uncomment if you hate log files or terminals and you want to punish them.
                    # for k, v in self.ranges_todo:
                    #     logging.debug("todo: %s:%s", k, v)

    def __load_minions(self):
        """ Read the previously active minions from the 'master/minions.json' file.
        These minions were active when the Master last stopped, and their timer wasn't expired.
        When the minions are read, their timeout Timer is started.
        """
        data = self.minion_persistor.ReadJSON()
        if data:
            with self.minions_lock:
                for minion in data:
                    self.minions.append(Minion(minion['target_hash'],
                                               PasswordRange(minion['range_start'],
                                                             minion['range_stop']),
                                               Timer(30, self.__minion_timeout)))
                if logging.RootLogger.manager.root.level == logging.DEBUG:
                    logging.debug("Loaded minions from file")
                    for minion in self.minions:
                        logging.debug("Minions: %s", minion.to_string())

    def __start_persistence(self):
        """ Start the file monitoring processes.

        Currently monitoring:
            input.json {input} -- Reading file for input hashes
            ranges_todo.json {output} -- Writing password range progress
        """
        logging.debug("Starting file monitoring processes")
        self.flush_progress_thread.start()
        self.input_monitor.start()

    def __flush_progress(self):
        """ Periodically write all hash cracking progress (self.ranges_todo list) to the
        'master/ranges_todo.json' file. Progress is written only when a change in the length
        of the {self.ranges_todo} list is detected.

        This method is meant to run in a multiprocessing.Process.

        The process will sleep for the amount of seconds stored in 'self.progress_flush_interval'.
        This value is also read from the master's 'conf.json' file on startup.
        Default value is 3 seconds.
        """
        logging.debug("Flush thread")
        prev_ranges_len = 0
        prev_minions_len = 0
        while True:
            with self.ranges_lock:
                if prev_ranges_len != len(self.ranges_todo):
                    result = self.ranges_todo_persistor.WriteJSON(self.ranges_todo)
                    logging.debug("JSON flush successful? %s", result)
                    prev_ranges_len = len(self.ranges_todo)
                else:
                    logging.debug("No change in ranges. Skipping flush.")
            with self.minions_lock:
                if prev_minions_len != len(self.minions):
                    str_minions = []
                    for minion in self.minions:
                        str_minions.append(minion.to_dict())
                    result = self.minion_persistor.WriteJSON(str_minions)
                    logging.debug("JSON flush successful? %s", result)
                    prev_minions_len = len(self.minions)
                else:
                    logging.debug("No change in minions. Skipping flush.")
            time.sleep(self.progress_flush_interval)

    def __monitor_input(self):
        """ Periodically read the 'master/input.json' file and handle new inputs.
        When new inputs are detected, they will be read and the file will be wiped.

        This method is meant to run in a multiprocessing.Process.

        The process will sleep for the amount of seconds stored in 'self.input_monitor_interval'.
        This value is also read from the master's 'conf.json' file on startup.
        Default value is 5 seconds.
        """
        while True:
            curr_content = self.input_persistor.Read()
            if curr_content:
                self.input_persistor.Write('')
                logging.debug(curr_content)
                try:
                    temp_hashes = json.loads(curr_content)
                    self.__handle_input_hashes(temp_hashes)
                except Exception as e:
                    logging.error('JSON parse error in input file: %s', e)
            elif curr_content:
                logging.debug('No change in input file')
            else:
                logging.debug("Read None from input file")
            time.sleep(self.input_monitor_interval)

    def __handle_input_hashes(self, input_hashes):
        """ Validate and filter hashes found in the '__monitor_input' method.

        Validations:
            Regex -- Each hash conforms to the MD5 output format

        Filters:
            Not cracked -- Each hash has not been cracked yet
                           (tracked via 'master/output.json' file)
            Not tracked -- Each hash is not being cracked right now
                           (tracked via 'self.ranges_todo'list)

        Arguments:
            input_hashes {list} -- List of hashes to be cracked

        Returns:
            list -- Filtered list of hashes
        """
        hashes = []
        pattern = re.compile("[a-fA-F0-9]{32}")
        # Filter hashes that conform to the MD5 format
        for temp in input_hashes:
            if pattern.fullmatch(temp):
                # Filter out hashes which are already cracked
                exists = False
                for found_hash in self.found_hashes:
                    if temp == found_hash['target_hash']:
                        exists = True
                        logging.debug("Hash already cracked: %s", temp)
                        break
                if not exists:
                    hashes.append(temp)
        # Filter out hashes which are currently being cracked (exist in the progress list)
        with self.ranges_lock:
            if self.ranges_todo:
                for k in self.ranges_todo:
                    if k[0] in hashes:
                        hashes.remove(k[0])
                        logging.debug("Hash already tracked in progress: %s", k[0])
        if hashes:
            self.__make_ranges(hashes)

    def __make_ranges(self, hashes):
        """ Create a list of password ranges for new hashes.
        Each range is sent to a Minion for cracking.

        The ranges will be between MIN_PASSWORD (0) and MAX_PASSWORD (99,999,999).
        This range of passwords will be sliced according to the amount of slices specified in
        'self.max_slices'. Example: if 'self.max_slices' is 10, this method will create 10 slices of
        ranges like: 0-9,999,999, 10,000,000-19,999,999, and so on.
        This value is read from the 'master/conf.json' file. Default: 1000 slices.

        Arguments:
            hashes {list} -- List of new hashes to crack. The range slices will be created
            for each hash.
        """
        logging.debug("Making ranges for new hashes: %s", hashes)
        increment = MAX_PASSWORD // self.max_slices
        with self.ranges_lock:
            for pw_hash in hashes:
                for i in range(1, self.max_slices):
                    self.ranges_todo.append((pw_hash, {
                        'range_start' : MIN_PASSWORD + (increment * (i - 1)),
                        'range_stop' : MIN_PASSWORD + (increment * i)
                    }))

                # For the last slice, if there's divison leftovers, add the slice with the leftovers
                # to the range list
                i += 1
                self.ranges_todo.append((pw_hash, {
                    'range_start' : MIN_PASSWORD + (increment * (i - 1)) + (MAX_PASSWORD % self.max_slices),
                    'range_stop' : MIN_PASSWORD + (increment * i) + (MAX_PASSWORD % self.max_slices)
                }))

                self._load_common_pw(pw_hash)
            
            # Debug print the todo list
            if logging.RootLogger.manager.root.level == logging.DEBUG:
                for k, v in self.ranges_todo:
                    logging.debug("todo: %s:%s", k, v)

    def _load_common_pw(self, target_hash):
        """ Append list of common passwords to the new ranges todo list.
        Note: This is a hardcoded simulation list of "common" passwords.
        """
        common_pw_file = JSONFilePersistor(os.path.join(os.path.dirname(__file__),
                                           'common_passwords.json'))
        common_pw = common_pw_file.ReadJSON()
        if common_pw:
            for pw in common_pw:
                self.ranges_todo.append((target_hash, {
                    'range_start' : GetPWRangeFromString(pw),
                    'range_stop' : GetPWRangeFromString(pw)
                }))

    def AddMinion(self):
        """ Handle a minion's request for something to compute, AKA an instruction.
        An instruction is a dict ready for JSON conversion containing the keys:

            'target_hash' -- The hash which needs to be cracked
            'range_start' -- Start of the password range to compare against 'target_hash'
            'range_stop' -- End of the password range

        If there's something to do (self.ranges_todo is not empty), the last range in the list is
        converted to a dict and returned to the Minion. The range sent to the minion is saved as
        a Minion instance in order to track which ranges are being tested by the minions.
        Before sending the instruction, the Minion instance's timeout timer is started.

        If there's nothing to do (self.ranges_todo is empty), the minion will receive
        an instruction with only the key: 'nothing_new', which will make him wait and ask the
        master again for an instruction.

        Returns:
            dict -- Instruction in the format specified above.
        """
        with self.ranges_lock:
            if not self.ranges_todo:
                instruction = {
                    "nothing_new" : 0
                }
                return instruction
            todo_tuple = self.ranges_todo.pop()

        todo = (todo_tuple[0], PasswordRange(todo_tuple[1]['range_start'],
                                             todo_tuple[1]['range_stop']))
        with self.minions_lock:
            minion = Minion(todo[0], todo[1], Timer(30, self.__minion_timeout))
            self.minions.append(minion)
        instruction = {
            "target_hash" : todo[0],
            "range_start" : todo[1].start,
            "range_stop"  : todo[1].stop
        }
        return instruction

    def ReportFailed(self, identifier):
        """ Handle a minion's report that its password range didn't contain the password
        which matches the target hash.
        Removes the minion using its identifier.

        Arguments:
            identifier {dict} -- Minion's representation in the style of a dict with the
            keys:

            'target_hash' -- The hash which needs to be cracked
            'range_start' -- Start of the password range
            'range_stop' -- End of the password range
        """
        self.__remove_minion(identifier=identifier)

    def ReportSuccess(self, identifier):
        """ Handle a minion's report that its password range contained the password
        which matches the target hash.
        The method calculates the MD5 hash of the received 'correct_pw' kwarg,
        and checks if they match. If the password indeed matches the target_hash,
        the method checks if there are any active minions working on this hash, and
        if there are ranges pertaining to the target_hash.
        If for some reason there weren't any active minions for this hash, and/or
        there weren't any todo ranges for this hash, the method concludes this report
        is a sham, and won't save the changes to the minions and ranges_todo lists,
        and the password will not be saved to the 'master/output.json' file.

        If there are active minions working on the hash, and there are todo ranges
        for this hash, both will be removed from the tracking lists and written to
        their respected files, and the correct password will be written to the
        'master/output.json' file.

        Arguments:
            identifier {dict} -- Minion's representation in the style of a dict with the
            keys:

            'target_hash' -- The hash which needs to be cracked
            'range_start' -- Start of the password range
            'range_stop' -- End of the password range
            'correct_pw' -- The password which matches the hash
        """
        # TODO: Change the way a hoax is detected according to the docstring
        with self.minions_lock:
            len_minions = len(self.minions)
        with self.ranges_lock:
            len_ranges = len(self.ranges_todo)

        # First remove this minion so he won't timeout while handling epic win
        self.__remove_minion(identifier=identifier)

        # Test if the password matches he hash
        if not CrackPW(identifier['correct_pw'], identifier['target_hash']):
            logging.error("Password doesn't match the hash.")
            return

        with self.minions_lock:
            # Remove all minions that are looking for the hash
            for i in RangeGenerator(len(self.minions)-1, 0, -1):
                if self.minions[i].target_hash == identifier['target_hash']:
                    self.__remove_minion(minion=self.minions[i])

            if len_minions <= len(self.minions):
                logging.warning("Didn't remove any minions. Is the success a hoax?")

        with self.ranges_lock:
            # Remove all todo ranges for the hash
            for i in RangeGenerator(len(self.ranges_todo)-1, 0, -1):
                if self.ranges_todo[i][0] == identifier['target_hash']:
                    self.ranges_todo.remove(self.ranges_todo[i])

            if len_ranges <= len(self.ranges_todo):
                logging.warning("Didn't remove any todo ranges. Is the success a hoax?")

            # Handle correct hash
            if len_minions > len(self.minions) and len_ranges > len(self.ranges_todo):
                logging.info("Found password %s for hash %s",
                             identifier['correct_pw'],
                             identifier['target_hash'])
                found_hash = {
                    'target_hash' : identifier['target_hash'],
                    'correct_pw' : identifier['correct_pw']
                }
                self.found_hashes.append(found_hash)
                self.output_persistor.AppendJSON(found_hash)

                if logging.RootLogger.manager.root.level == logging.DEBUG:
                    logging.debug("Minion Queue: %s", self.minions)
                    logging.debug("Ranges Queue: %s", self.ranges_todo)
                    for v in self.minions:
                        logging.debug("Minion: %s, %s-%s", v.target_hash, v.pw_range.start, v.pw_range.stop)
                    for v in self.ranges_todo:
                        logging.debug("Range: %s, %s", v[0], v[1])

    def __minion_timeout(self, minion):
        """ When a minion hasn't reported to the master after a given timeout time (30 seconds),
        it is removed from the active minions list, and its allocated password range is returned
        to the 'self.ranges_todo' list.

        Arguments:
            minion {Minion} -- The minion instance to be removed
        """
        logging.info("Minion timeout: %s:%s-%s", minion.target_hash, minion.pw_range.start, minion.pw_range.stop)
        try:
            with self.minions_lock:
                self.minions.remove(minion)
            with self.ranges_lock:
                # Add back the range that the minion didn't finish checking
                self.ranges_todo.append((minion.target_hash, {
                    'range_start' : minion.pw_range.start,
                    'range_stop' : minion.pw_range.stop
                }))
        except Exception:
            logging.error("Can't find minion %s", minion.to_string())

    def __remove_minion(self, identifier: dict = None, minion: Minion = None):
        """ Find the minion in the active minions list and remove it.
        A minion can be identified as a dict via the 'identifier' argument,
        or as a Minion instance via the 'minion' argument.

        Keyword Arguments:
            identifier {dict} -- (default: {None}) Representation of the minion in a dict
            containing the keys:

                'target_hash' -- The hash which needs to be cracked
                'range_start' -- Start of the password range
                'range_stop' -- End of the password range

            minion {Minion} -- (default: {None}) Representation of a minion as a Minion
            instance
        """
        if identifier:
            minion = self.__find_minion(identifier)
        if not minion:
            if identifier:
                desc = (identifier['target_hash'],
                        PasswordRange(identifier['range_start'], identifier['range_stop']))
                logging.warning("Tried removing non-existant minion %s:%s-%s",
                                desc[0],
                                desc[1].start,
                                desc[1].stop)
            else:
                logging.warning("Tried removing non-existant minion")
            return

        try:
            minion.timer.cancel()
            self.minions.remove(minion)
        except Exception:
            logging.warning("Can't find minion %s", minion.to_string())

    def __find_minion(self, identifier):
        """ Find a minion in the active minions list using a kwarg identifier.

        Arguments:
            identifier {dict} -- Representation of the minion in a dict
            containing the keys:

                'target_hash' -- The hash which needs to be cracked
                'range_start' -- Start of the password range
                'range_stop' -- End of the password range

        Returns:
            Minion -- The Minion instance from the active minions list
            None -- If the minion is not in the active minions list
        """
        for minion in self.minions:
            if (minion.target_hash == identifier['target_hash'] and
                    minion.pw_range.start == identifier['range_start'] and
                    minion.pw_range.stop == identifier['range_stop']):
                return minion
        return None
