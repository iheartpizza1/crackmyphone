""" Master's server module. This is the starting point of the Master.
"""
import json
import logging
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../'))
from tools.tools import ValidateJSON, RangeGenerator
from tools.persistence import JSONFilePersistor
from http.server import BaseHTTPRequestHandler, HTTPServer
from tools.cypher import Base64CypherCreator
from minion_handler import MinionHandler

HOST = "localhost"
PORT = "8080"
MAX_SLICE = 1000
_minion_handler = None
_cypher = Base64CypherCreator()

class Server(BaseHTTPRequestHandler):
    """ Implementation of Python's builtin HTTP server. Handles HTTP communication.
    """
    def do_GET(self):
        """ Handle GET requests
        """
        ### /instruction ###
        if self.path == '/instruction':
            instruction = _minion_handler.AddMinion()
            body = json.dumps(instruction, indent=2)
            self.send_response(200)
        ### Else ###
        else:
            self.send_response(404)
        self.end_headers()
        if body:
            self.wfile.write(bytes(_cypher.encode(body), 'utf-8'))

    def do_POST(self):
        """ Handle POST requests
        """
        ### /notfound ###
        if self.path == '/notfound':
            identifier = self.__extract_response_json()

            # If the JSON did not pass validation, return error to minion
            if not identifier:
                self.send_error(400)
            else:
                logging.info("Minion reporting not found: %s:%s-%s",
                             identifier['target_hash'],
                             identifier['range_start'],
                             identifier['range_stop'])

                # Handle unsuccessful match
                _minion_handler.ReportFailed(identifier)
                self.send_response(200)
        ### /found ###
        elif self.path == '/found':
            identifier = self.__extract_response_json()

            # If the JSON did not pass validation, return error to minion
            if not identifier or (identifier and not 'correct_pw' in identifier):
                self.send_error(400)
            else:
                logging.info("Minion reporting found: %s:%s-%s",
                             identifier['target_hash'],
                             identifier['range_start'],
                             identifier['range_stop'])

                try:
                    # Handle successful match
                    _minion_handler.ReportSuccess(identifier)
                    self.send_response(200)
                except Exception:
                    self.send_error(400)
        ### Else ###
        else:
            self.send_response(404)
        self.end_headers()

    def __extract_response_json(self):
        """ Extract response to a JSON dict

        Returns:
            dict -- JSON parse result after validation, or None if validation failed
        """
        # Extract content length from headers
        for header in self.headers.raw_items():
            if header[0].lower() == 'content-length':
                size = int(header[1])
                break

        data = self.rfile.read(size)
        logging.debug("Encoded msg: %s", data)
        json_data = _cypher.decode(data)
        return ValidateJSON(json_data)

def StartServer():
    """ Create an instance of HTTP Server and serve it forever
    """
    httpd = HTTPServer((HOST, PORT), Server)
    logging.info("Server started at %s:%s", HOST, PORT)
    httpd.serve_forever()

def __load_conf():
    """ Load a 'conf.json' file, and distribute global attributes.

    Returns:
        dict -- kwargs read from the conf file
    """
    conf_persistor = JSONFilePersistor(os.path.join(os.path.dirname(__file__), 'conf.json'))
    conf_dict = conf_persistor.ReadJSON()
    if not conf_dict:
        return None

    if 'host' in conf_dict:
        global HOST
        HOST = conf_dict['host']

    if 'port' in conf_dict:
        global PORT
        PORT = conf_dict['port']

    if 'max_slice' in conf_dict:
        global MAX_SLICE
        MAX_SLICE = conf_dict['max_slice']

    return conf_dict

def _parse_args(log_kwargs):
    """ Parse script's args for logger settings.

    Arguments:
        log_kwargs {dict} -- kwargs for the logger, set first by the conf file.

    Raises:
        ValueError -- When an arg is missing its value or is invalid for the logger' settings

    Returns:
        dict -- Modified log_kwargs for the logger
    """
    numeric_level = log_kwargs['level']
    if "--log" in sys.argv:
        try:
            for i in RangeGenerator(1, len(sys.argv)):
                if (sys.argv[i] == "--log" and
                        i+1 < len(sys.argv) and
                        sys.argv[i+1]):
                    loglevel = sys.argv[i+1]
                    break
        except IndexError:
            raise ValueError('Invalid log level')
        if loglevel:
            numeric_level = getattr(logging, loglevel.upper(), None)
            if not isinstance(numeric_level, int):
                raise ValueError('Invalid log level: %s' % loglevel)
    log_kwargs['level'] = numeric_level
    if "--logfile-name" in sys.argv:
        try:
            for i in RangeGenerator(1, len(sys.argv)):
                if (sys.argv[i] == "--logfile-name" and sys.argv[i+1]):
                    log_kwargs['filename'] = sys.argv[i+1]
                    break
        except IndexError:
            raise ValueError('Invalid log file name')

    return log_kwargs

def main():
    """ Main function for the Master. Load settings from the conf file,
    check for user arguments for the logger, and start the server module.
    """
    conf_dict = __load_conf()
    log_kwargs = {}
    if "loglevel" in conf_dict:
        log_kwargs['level'] = getattr(logging, conf_dict['loglevel'].upper(), None)
    if "filename" in conf_dict:
        log_kwargs['filename'] = conf_dict['filename']
    if "format" in conf_dict:
        log_kwargs['format'] = conf_dict['format']

    log_kwargs = _parse_args(log_kwargs)

    logging.basicConfig(**log_kwargs)

    args = [MAX_SLICE]
    if conf_dict and 'progress_flush_interval' in conf_dict:
        args.append(conf_dict['progress_flush_interval'])
    if conf_dict and "input_monitor_interval" in conf_dict:
        args.append(conf_dict["input_monitor_interval"])
    global _minion_handler
    _minion_handler = MinionHandler(*args)
    StartServer()

if __name__ == '__main__':
    main()
