# CrackMyPhone

Password cracking system composed of a Master server which handles input, output and instruction assignment to one or more Minion
clients. Each minion is configured by default to run 2 processes which handle the MD5 password hash computations and comparisons,
but this setting can be changed in the Minion's conf.json file.

Input hashes are ingested from the /master/input.json file. If the hashes aren't being cracked or they aren't already cracked,
the master creates a list of password ranges (slices) which will be received by the minions and compared to the target hash.

Also, in order to improve the chance of an early success, a list of "common passwords" (ironic quotation marks) is loaded
before the list of password ranges, which makes the minions compute these passwords very fast.

#### Minion Workflow
Minion requests instruction from master -> Minion receives an instruction -> Divide the ranges to be executed by the set number
of processes -> Processes compute MD5 hash for each password in its range, compare it to the target hash -> results are sent
to the master

# Requirements

+ Python 3.7.2 64-bit

+ The Python's installation directory is in your environment's PATH variable


# Settings
### Master

The master has a number of configurations available through the conf.json file:

+ _Host_ - IP address to which the master's HTTP server will be bound. Must be either _localhost_ or the machines configured IP.

+ _Port_ - Network por to which the master's HTTP server will be bound. Must be an available port on the machine.

+ _Loglevel_ - The severity setting for the master's logger.

+ _Format_ - The logger's message format.

+ _Filename_ - The log file name to which all logs will be written instead of stdout.

+ _Max_slice_ - The amount of slices which will be made for each new hash in the range of 0-99,999,999  
  (Each minion gets such slice to go through, so too many slices will result in very frequent requests to the maser).
 
+ _Progress_flush_interval_ - Number of seconds between each write of the cracking progress to the /master/ranges_todo.json file.

+ _Input_monitor_interval_ - Number of seconds between each read from the /master/input.json file.

### Minion

+ _Server_ - IP address and port to which the minion's HTTP client will be bound. Must match the master's configrued IP and port.  
   Format: **1.1.1.1:8080**  
   
+ _Thread_count_ - Number of processes which will do the computations for each minion.

+ _Loglevel_ - The severity setting for the master's logger.

+ _Format_ - The logger's message format.

+ _Filename_ - The log file name to which all logs will be written instead of stdout.


### Command-line Arguments

Both the master and the minions can accept command-line arguments:

+ --log <severity> - Sets the logger's severity. Allowed values: DEBUG, INFO, WARNING, ERROR, FATAL

+ --logfile-name <file name> - The log file name to which all logs will be written instead of stdout.

**Note:** Command-line arguments take precedence on conf file settings.

# How To Start

1. Open the master's and minion's _conf.json_ files and set the appropriate IP and port settings.

2. Open a command shell in the /master directory, and run _python server.py_.

3. Open one or more command shells in the /minion directory, and run _python client.py_.

That's it, the minions are doing the master's work. Feel free to try to close either of the shells, they can handle it.


# Input

Input to the master is handled via the /master/input.json file. It's provided with two sample hashes in this repository.
The master expects a JSON formatted array of MD5 hashes. When they are read by the master, the file is wiped. Don't worry though,
cracking progress is occasionally written to other files.


# Output

Cracked passwords are saved with their hashes in the /master/output.json file.


# Tests

Included are a couple of unittest scripts in the /tests directory. 

To use them, _cd_ to the directory and run either _python master_tests.py_ or _python persistence_tests.py_.
For the _master_tests.py_ script to work right, you must edit it and change the value of _MASTER_SERVER_
to the IP and port you've set for the master in its conf file.

Please note, these tests are not comprehensive.

They are merely a proof of concept for the time-frame given for the project.